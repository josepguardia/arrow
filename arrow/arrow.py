# -*- coding: utf-8 -*-
'''
Provides the :class:`Arrow <arrow.arrow.Arrow>` class, an enhanced ``datetime``
replacement.

JG: split datetime and subsconds
'''

from __future__ import absolute_import
import os
import re

from datetime import datetime, timedelta, tzinfo, timezone
from dateutil import tz as dateutil_tz
from dateutil.parser import parse as dateparser
from dateutil.relativedelta import relativedelta
from dateutil.tz import tzutc
import more_itertools as mit
import numpy as np
import collections

# from classproperties import classproperty

class classproperty(object):
  def __init__(self, func):
    self.func = func

  def __get__(self, obj, owner):
    args = []

    if self.func.__code__.co_argcount > 0:
      args.append(owner)

    return self.func(*args)

import math
from math import trunc, log10
import calendar
import sys
import warnings
from decimal import *
from pint import UnitRegistry
ureg = UnitRegistry()

dir_path = os.path.dirname(os.path.realpath(__file__))
ureg.load_definitions(os.path.join(dir_path, 'custom_def.txt'))

getcontext().prec = 54
# getcontext().rounding = ROUND_05UP

from arrow import util, locales, parser, formatter
from sty import fg, bg, ef, rs


class Arrow(object):
    '''An :class:`Arrow <arrow.arrow.Arrow>` object.

    Implements the ``datetime`` interface, behaving as an aware ``datetime`` while implementing
    additional functionality.

    :param year: the calendar year.
    :param month: the calendar month.
    :param day: the calendar day.
    :param hour: (optional) the hour. Defaults to 0.
    :param minute: (optional) the minute, Defaults to 0.
    :param second: (optional) the second, Defaults to 0.
    :param microsecond: (optional) the microsecond. Defaults 0.
    :param tzinfo: (optional) A timezone expression.  Defaults to UTC.

    .. _tz-expr:

    Recognized timezone expressions:

        - A ``tzinfo`` object.
        - A ``str`` describing a timezone, similar to 'US/Pacific', or 'Europe/Berlin'.
        - A ``str`` in ISO-8601 style, as in '+07:00'.
        - A ``str``, one of the following:  'local', 'utc', 'UTC'.

    Usage::

        >>> import arrow
        >>> arrow.Arrow(2013, 5, 5, 12, 30, 45)
        <Arrow [2013-05-05T12:30:45+00:00]>

    '''

    resolution = datetime.resolution

    # 'yottayear', 'zettayear', 'exayear', 'petayear', 'terayear', 'gigayear', 'megayear', 'kiloyear',

    _ATTRS = ['year', 'month', 'day', 'hour', 'minute', 'second', 'millisecond',
              'microsecond', 'nanosecond', 'picosecond', 'femtosecond',
              'attosecond', 'zeptosecond', 'yoctosecond']


    _ATTRS_PLURAL = ['{0}s'.format(a) for a in _ATTRS]
    _MONTHS_PER_QUARTER = 3


    def __init__(self,
                 value: Decimal = Decimal(0),
                 precision: int = 6):
        '''
        Value is a UnixTimestamp
        '''

        self.value = value.quantize(Decimal('10E-{}'.format(precision)))
        self.precision = precision

    @classmethod
    def fromDatetime(cls,
                     datetime: datetime = datetime.utcfromtimestamp(0),
                     subseconds: Decimal = None,
                     precision: int = 6):

        value, _ = divmod(Decimal(datetime.timestamp()), Decimal(1.0))
        # Afegim els subsegons...
        if not subseconds is None:
            if value.is_signed():
                value -= (Decimal(0) - subseconds)
            else:
                value += subseconds
        else:
            x = (ureg.microsecond * datetime.microsecond).to("second")
            if value.is_signed() and datetime.microsecond > 0:
                value -= (Decimal(1) - Decimal(str(x.magnitude)))
            else:
                value += Decimal(str(x.magnitude))


        return cls(value, precision)

    @classmethod
    def fromISO(cls, str, precision=1):
        p = re.compile('^(?P<datetime>[^\.]*)\.?(?P<subseconds>[0-9]*)$')
        m = p.match(str)

        datetime_str = m.group('datetime')
        subsecond_str = "0." + m.group('subseconds')
        precision = max(len(m.group('subseconds') or ""), precision)

        dt = dateparser(datetime_str)
        subseconds = Decimal(subsecond_str)

        return cls.fromDatetime(datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, tzinfo=dt.tzinfo), subseconds, precision)


    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value #.quantize(Decimal('10E-{}'.format(self.precision)))

    @property
    def datetime(self) -> datetime:
        '''
        Returns a datetime representation of the :class:`Arrow <arrow.arrow.Arrow>` object.
        with microseconds == 0
        '''
        if self.value.is_signed():
            integer = self.value.to_integral_value(rounding=ROUND_FLOOR)
        else:
            integer = self.value.to_integral_value(rounding=ROUND_DOWN)

        return datetime.fromtimestamp(integer, tz=tzutc()) #+ r

    @property
    def subseconds(self):
        '''
        '''
        integer, decimal = divmod(self.value, Decimal(1.0))
        if integer.is_signed():
            decimal = Decimal(1) - abs(decimal)

        return decimal

    @property
    def julian(self):
        return (self.value / Decimal(86400.0)) + Decimal(2440587.5)


    Level = collections.namedtuple('Level', ['master', 'level', 'sublevel', 'frame', 'magnitude', 'quantity'], rename=False, defaults=[None] * 6)
    Mark = collections.namedtuple('Mark', ['time', 'i', 'j', 'text', 'level'], rename=False)

    # # @profile
    # # @timecall(immediate=True)
    # def rulerize(self, base_level : Level):
    #     '''
    #     Retorna el format a mostrar...
    #     '''
    #     frame_absolute, frame_relative, relative_steps, _ = self._get_frames(frame)
    #     index = self._ATTRS.index(frame_absolute)
    #     frames = self._ATTRS[:index + 1]
    #
    #     for f in frames:
    #         if self.floor(f) == self:
    #             frame = f
    #             break
    #
    #     if frame_relative != frame_absolute:
    #         base_units = ureg.parse_expression(frame_relative).units
    #
    #         m1 = ureg.parse_expression(frame_relative).magnitude
    #         m2 = ureg.parse_expression(frame_absolute).to(base_units).magnitude
    #
    #         index += (int(log10(m1) - log10(m2)) // 3)
    #
    #
    #     return getattr(self, frame), frame, index - self._ATTRS.index(frame)

    @classmethod
    def ruler(cls, start, end, width=1024, space=20, rulerize=False):
        base_level = cls.bestFit(start, end, width, space)
        l = []

        start = start.floor(base_level.frame, base_level.magnitude)
        for x in list(Arrow.range(frame=base_level.frame,
                                  step=base_level.magnitude,
                                  start=start,
                                  end=end)):


            i = base_level.master
            if x.value != Decimal("0.0"):
                while x.value % cls.levels[i].quantity.magnitude == Decimal("0.0"):
                    i = i - 1


            if cls.levels[i+1].frame == "day":
                if getattr(x, cls.levels[i+1].frame) == 1:
                    i = i - 1
            if cls.levels[i+1].frame == "month":
                if getattr(x, cls.levels[i+1].frame) == 1:
                    i = i - 1
            if cls.levels[i+1].frame == "year":
                year = getattr(x, cls.levels[i+1].frame)
                for j, z in enumerate([1, 5, 10, 50, 100, 500, 1000]):
                    if year % z != 0:
                        break

                i = i - (j - 1)

            level = cls.levels[i+1]
            l.append(cls.Mark(time=x,
                              i=base_level.master - level.master,
                              j=base_level.level - level.level,
                              text=str(getattr(x, level.frame)),
                              level=level))
        return base_level, l


    @classmethod
    def bestFit(cls, start, end, width=1024, space=20):
        # print("start", start)
        # print("end", end)
        # print("width", width)


        s = Decimal(end.value - start.value) * ureg.seconds / Decimal(width / space)

        def fx(l, s):
            return l.quantity > s

        return next((l for l in reversed(cls.levels) if fx(l, s)))


    @classproperty
    def levels(cls):
        if not hasattr(cls, '_levels'):
            cls._levels = []
            for i, attr in enumerate(cls._ATTRS):
                frame, _, _, subs = cls._get_frames(attr)
                # print (attr, frame, subs)
                for j, sub in enumerate(reversed(subs)):
                    x = (ureg.parse_expression(frame) * Decimal(sub)).to_base_units()
                    cls._levels.append(cls.Level(master=len(cls._levels),
                                                 level=i,
                                                 sublevel=j,
                                                 frame=frame,
                                                 magnitude=sub,
                                                 quantity=x))
        return cls._levels

    @classmethod
    def now(cls, tzinfo=None):
        '''Constructs an :class:`Arrow <arrow.arrow.Arrow>` object, representing "now" in the given
        timezone.

        :param tzinfo: (optional) a ``tzinfo`` object. Defaults to local time.

        '''

        tzinfo = tzinfo if tzinfo is not None else dateutil_tz.tzlocal()
        dt = datetime.now(tzinfo)

        return cls(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond, tzinfo=dt.tzinfo)


    @classmethod
    def utcnow(cls):
        ''' Constructs an :class:`Arrow <arrow.arrow.Arrow>` object, representing "now" in UTC
        time.

        '''

        dt = datetime.now(dateutil_tz.tzutc())


        return cls(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond, dt.tzinfo)

    @classmethod
    def fromtimestamp(cls, timestamp, tzinfo=None):
        ''' Constructs an :class:`Arrow <arrow.arrow.Arrow>` object from a timestamp, converted to
        the given timezone.

        :param timestamp: an ``int`` or ``float`` timestamp, or a ``str`` that converts to either.
        :param tzinfo: (optional) a ``tzinfo`` object.  Defaults to local time.

        Timestamps should always be UTC. If you have a non-UTC timestamp::

            >>> arrow.Arrow.utcfromtimestamp(1367900664).replace(tzinfo='US/Pacific')
            <Arrow [2013-05-07T04:24:24-07:00]>

        '''

        tzinfo = tzinfo if tzinfo is not None else dateutil_tz.tzlocal()
        timestamp = cls._get_timestamp_from_input(timestamp)
        dt = datetime.fromtimestamp(timestamp, tzinfo)
        subsecond = dt.microsecond / 1000000

        return cls(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, subsecond, dt.tzinfo)


    @classmethod
    def utcfromtimestamp(cls, timestamp):
        '''Constructs an :class:`Arrow <arrow.arrow.Arrow>` object from a timestamp, in UTC time.

        :param timestamp: an ``int`` or ``float`` timestamp, or a ``str`` that converts to either.

        '''

        timestamp = cls._get_timestamp_from_input(timestamp)
        dt = datetime.utcfromtimestamp(timestamp)
        subsecond = dt.microsecond / 1000000

        return cls(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, subsecond, dateutil_tz.tzutc())

    # @classmethod
    # def fromdatetime(cls, dt, tzinfo=None):
    #     ''' Constructs an :class:`Arrow <arrow.arrow.Arrow>` object from a ``datetime`` and
    #     optional replacement timezone.
    #
    #     :param dt: the ``datetime``
    #     :param tzinfo: (optional) A :ref:`timezone expression <tz-expr>`.  Defaults to ``dt``'s
    #         timezone, or UTC if naive.
    #
    #     If you only want to replace the timezone of naive datetimes::
    #
    #         >>> dt
    #         datetime.datetime(2013, 5, 5, 0, 0, tzinfo=tzutc())
    #         >>> arrow.Arrow.fromdatetime(dt, dt.tzinfo or 'US/Pacific')
    #         <Arrow [2013-05-05T00:00:00+00:00]>
    #
    #     '''
    #     if tzinfo is None:
    #         if dt.tzinfo is None:
    #             tzinfo = dateutil_tz.tzutc()
    #         else:
    #             tzinfo = dt.tzinfo
    #
    #     return cls(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond, tzinfo)

    @classmethod
    def fromdate(cls, date, tzinfo=None):
        ''' Constructs an :class:`Arrow <arrow.arrow.Arrow>` object from a ``date`` and optional
        replacement timezone.  Time values are set to 0.

        :param date: the ``date``
        :param tzinfo: (optional) A :ref:`timezone expression <tz-expr>`.  Defaults to UTC.
        '''

        tzinfo = tzinfo if tzinfo is not None else dateutil_tz.tzutc()

        return cls(date.year, date.month, date.day, tzinfo=tzinfo)

    @classmethod
    def strptime(cls, date_str, fmt, tzinfo=None):
        ''' Constructs an :class:`Arrow <arrow.arrow.Arrow>` object from a date string and format,
        in the style of ``datetime.strptime``.  Optionally replaces the parsed timezone.

        :param date_str: the date string.
        :param fmt: the format string.
        :param tzinfo: (optional) A :ref:`timezone expression <tz-expr>`.  Defaults to the parsed
            timezone if ``fmt`` contains a timezone directive, otherwise UTC.

        '''

        dt = datetime.strptime(date_str, fmt)
        tzinfo = tzinfo if tzinfo is not None else dt.tzinfo

        subsecond = dt.microsecond / 1000000

        return cls(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, subsecond, tzinfo)


    # factories: ranges and spans

    @classmethod
    @util.list_to_iter_deprecation
    def range(cls, frame, start, end=None, tz=None, limit=10000, step=1):
        ''' Returns an iterator of :class:`Arrow <arrow.arrow.Arrow>` objects, representing
        points in time between two inputs.

        :param frame: The timeframe.  Can be any ``datetime`` property (day, hour, minute...).
        :param start: A datetime expression, the start of the range.
        :param end: (optional) A datetime expression, the end of the range.
        :param tz: (optional) A :ref:`timezone expression <tz-expr>`.  Defaults to
            ``start``'s timezone, or UTC if ``start`` is naive.
        :param limit: (optional) A maximum number of tuples to return.

        **NOTE**: The ``end`` or ``limit`` must be provided.  Call with ``end`` alone to
        return the entire range.  Call with ``limit`` alone to return a maximum # of results from
        the start.  Call with both to cap a range at a maximum # of results.

        **NOTE**: ``tz`` internally **replaces** the timezones of both ``start`` and ``end`` before
        iterating.  As such, either call with naive objects and ``tz``, or aware objects from the
        same timezone and no ``tz``.

        Supported frame values: year, quarter, month, week, day, hour, minute, second.

        Recognized datetime expressions:

            - An :class:`Arrow <arrow.arrow.Arrow>` object.
            - A ``datetime`` object.

        Usage::

            >>> start = datetime(2013, 5, 5, 12, 30)
            >>> end = datetime(2013, 5, 5, 17, 15)
            >>> for r in arrow.Arrow.range('hour', start, end):
            ...     print(repr(r))
            ...
            <Arrow [2013-05-05T12:30:00+00:00]>
            <Arrow [2013-05-05T13:30:00+00:00]>
            <Arrow [2013-05-05T14:30:00+00:00]>
            <Arrow [2013-05-05T15:30:00+00:00]>
            <Arrow [2013-05-05T16:30:00+00:00]>

        **NOTE**: Unlike Python's ``range``, ``end`` *may* be included in the returned iterator::

            >>> start = datetime(2013, 5, 5, 12, 30)
            >>> end = datetime(2013, 5, 5, 13, 30)
            >>> for r in arrow.Arrow.range('hour', start, end):
            ...     print(repr(r))
            ...
            <Arrow [2013-05-05T12:30:00+00:00]>
            <Arrow [2013-05-05T13:30:00+00:00]>

        '''

        frame_absolute, frame_relative, relative_steps, _ = cls._get_frames(frame)
        if frame_relative == 'second' and frame_relative != frame_absolute:
            base_units = ureg.parse_expression(frame_relative).units

            delta = Decimal(str(ureg.parse_expression(frame_absolute).to(base_units).magnitude))
        else:
            frame_relative += "s"
            delta = relativedelta(**{frame_relative: relative_steps})

        delta *= step
        tzinfo = cls._get_tzinfo(start.tzinfo if tz is None else tz)

        end, limit = cls._get_iteration_params(end, limit)

        current, i = start, 0
        while current <= end and i < limit:
            i += 1
            yield current

            current = current + delta

    @classmethod
    @util.list_to_iter_deprecation
    def span_range(cls, frame, start, end, tz=None, limit=None):
        ''' Returns an iterator of tuples, each :class:`Arrow <arrow.arrow.Arrow>` objects,
        representing a series of timespans between two inputs.

        :param frame: The timeframe.  Can be any ``datetime`` property (day, hour, minute...).
        :param start: A datetime expression, the start of the range.
        :param end: (optional) A datetime expression, the end of the range.
        :param tz: (optional) A :ref:`timezone expression <tz-expr>`.  Defaults to
            ``start``'s timezone, or UTC if ``start`` is naive.
        :param limit: (optional) A maximum number of tuples to return.

        **NOTE**: The ``end`` or ``limit`` must be provided.  Call with ``end`` alone to
        return the entire range.  Call with ``limit`` alone to return a maximum # of results from
        the start.  Call with both to cap a range at a maximum # of results.

        **NOTE**: ``tz`` internally **replaces** the timezones of both ``start`` and ``end`` before
        iterating.  As such, either call with naive objects and ``tz``, or aware objects from the
        same timezone and no ``tz``.

        Supported frame values: year, quarter, month, week, day, hour, minute, second.

        Recognized datetime expressions:

            - An :class:`Arrow <arrow.arrow.Arrow>` object.
            - A ``datetime`` object.

        **NOTE**: Unlike Python's ``range``, ``end`` will *always* be included in the returned
        iterator of timespans.

        Usage:

            >>> start = datetime(2013, 5, 5, 12, 30)
            >>> end = datetime(2013, 5, 5, 17, 15)
            >>> for r in arrow.Arrow.span_range('hour', start, end):
            ...     print(r)
            ...
            (<Arrow [2013-05-05T12:00:00+00:00]>, <Arrow [2013-05-05T12:59:59.999999+00:00]>)
            (<Arrow [2013-05-05T13:00:00+00:00]>, <Arrow [2013-05-05T13:59:59.999999+00:00]>)
            (<Arrow [2013-05-05T14:00:00+00:00]>, <Arrow [2013-05-05T14:59:59.999999+00:00]>)
            (<Arrow [2013-05-05T15:00:00+00:00]>, <Arrow [2013-05-05T15:59:59.999999+00:00]>)
            (<Arrow [2013-05-05T16:00:00+00:00]>, <Arrow [2013-05-05T16:59:59.999999+00:00]>)
            (<Arrow [2013-05-05T17:00:00+00:00]>, <Arrow [2013-05-05T17:59:59.999999+00:00]>)

        '''

        tzinfo = cls._get_tzinfo(start.tzinfo if tz is None else tz)
        start = cls.fromdatetime(start, tzinfo).span(frame)[0]
        _range = cls.range(frame, start, end, tz, limit)
        return (r.span(frame) for r in _range)

    @classmethod
    @util.list_to_iter_deprecation
    def interval(cls, frame, start, end, interval=1, tz=None):
        ''' Returns an iterator of tuples, each :class:`Arrow <arrow.arrow.Arrow>` objects,
        representing a series of intervals between two inputs.

        :param frame: The timeframe.  Can be any ``datetime`` property (day, hour, minute...).
        :param start: A datetime expression, the start of the range.
        :param end: (optional) A datetime expression, the end of the range.
        :param interval: (optional) Time interval for the given time frame.
        :param tz: (optional) A timezone expression.  Defaults to UTC.

        Supported frame values: year, quarter, month, week, day, hour, minute, second

        Recognized datetime expressions:

            - An :class:`Arrow <arrow.arrow.Arrow>` object.
            - A ``datetime`` object.

        Recognized timezone expressions:

            - A ``tzinfo`` object.
            - A ``str`` describing a timezone, similar to 'US/Pacific', or 'Europe/Berlin'.
            - A ``str`` in ISO-8601 style, as in '+07:00'.
            - A ``str``, one of the following:  'local', 'utc', 'UTC'.

        Usage:

            >>> start = datetime(2013, 5, 5, 12, 30)
            >>> end = datetime(2013, 5, 5, 17, 15)
            >>> for r in arrow.Arrow.interval('hour', start, end, 2):
            ...     print r
            ...
            (<Arrow [2013-05-05T12:00:00+00:00]>, <Arrow [2013-05-05T13:59:59.999999+00:00]>)
            (<Arrow [2013-05-05T14:00:00+00:00]>, <Arrow [2013-05-05T15:59:59.999999+00:00]>)
            (<Arrow [2013-05-05T16:00:00+00:00]>, <Arrow [2013-05-05T17:59:59.999999+00:0]>)
        '''
        if interval < 1:
            raise ValueError("interval has to be a positive integer")

        spanRange = iter(cls.span_range(frame, start, end, tz))
        while True:
            intvlStart, intvlEnd = next(spanRange)  # StopIteration when exhausted
            for _ in range(interval-1):
                _, intvlEnd = next(spanRange)  # StopIteration when exhausted
            yield intvlStart, intvlEnd

    # representations

    def __repr__(self):
        return '<{0} [{1}]>'.format(self.__class__.__name__, self.__str__())

    def __str__(self):
        subseconds = "{1:.{0}f}".format(self.precision,
                                        abs(self.subseconds))

        return '{0:%Y}-{0:%m}-{0:%d} {0:%H}:{0:%M}:{0:%S}.{1} {0:%z}'.format(self.datetime, subseconds[2:])



    def __format__(self, formatstr):

        if len(formatstr) > 0:
            return self.format(formatstr)

        return str(self)

    def __hash__(self):
        return self.datetime.__hash__()


    # attributes & properties

    def __getattr__(self, name):
        if name == 'week':
            return self.isocalendar()[1]

        if name == 'quarter':
            return int((self.month-1)/self._MONTHS_PER_QUARTER) + 1

        if name.endswith("year") and len(name) > len("year"):
            x = (ureg.parse_expression("year") * self.subseconds).to(name)
            return int(x.magnitude) % 1000

        if name.endswith("second") and len(name) > len("second"):
            x = (ureg.parse_expression("second") * self.subseconds).to(name)
            return int(x.magnitude) % 1000

        if not name.startswith('_'):

            value = getattr(self.datetime, name, None)
            if value is not None:
                return value

        return object.__getattribute__(self, name)

    @property
    def tzinfo(self):
        ''' Gets the ``tzinfo`` of the :class:`Arrow <arrow.arrow.Arrow>` object. '''

        return self.datetime.tzinfo

    @tzinfo.setter
    def tzinfo(self, tzinfo):
        ''' Sets the ``tzinfo`` of the :class:`Arrow <arrow.arrow.Arrow>` object. '''

        self.datetime = self.datetime.replace(tzinfo=tzinfo)


    @property
    def naive(self):
        ''' Returns a naive datetime representation of the :class:`Arrow <arrow.arrow.Arrow>`
        object. '''

        return self.datetime.replace(tzinfo=None)

    @property
    def timestamp(self):
        ''' Returns a timestamp representation of the :class:`Arrow <arrow.arrow.Arrow>` object, in
        UTC time. '''

        return calendar.timegm(self.datetime.utctimetuple())

    @property
    def float_timestamp(self):
        ''' Returns a floating-point representation of the :class:`Arrow <arrow.arrow.Arrow>`
        object, in UTC time. '''

        return self.timestamp + float(self._subsecond)


    # mutation and duplication.

    def clone(self):
        ''' Returns a new :class:`Arrow <arrow.arrow.Arrow>` object, cloned from the current one.

        Usage:

            >>> arw = arrow.utcnow()
            >>> cloned = arw.clone()

        '''

        return self.fromdatetime(self.datetime)

    def replace(self, **kwargs):
        ''' Returns a new :class:`Arrow <arrow.arrow.Arrow>` object with attributes updated
        according to inputs.

        Use property names to set their value absolutely::

            >>> import arrow
            >>> arw = arrow.utcnow()
            >>> arw
            <Arrow [2013-05-11T22:27:34.787885+00:00]>
            >>> arw.replace(year=2014, month=6)
            <Arrow [2014-06-11T22:27:34.787885+00:00]>

        You can also replace the timezone without conversion, using a
        :ref:`timezone expression <tz-expr>`::

            >>> arw.replace(tzinfo=tz.tzlocal())
            <Arrow [2013-05-11T22:27:34.787885-07:00]>

        '''

        absolute_kwargs = {}
        relative_kwargs = {}  # TODO: DEPRECATED; remove in next release

        for key, value in kwargs.items():

            if key in self._ATTRS:
                absolute_kwargs[key] = value
            elif key in self._ATTRS_PLURAL or key in ['weeks', 'quarters']:
                # TODO: DEPRECATED
                warnings.warn("replace() with plural property to shift value "
                              "is deprecated, use shift() instead",
                              DeprecationWarning)
                relative_kwargs[key] = value
            elif key in ['week', 'quarter']:
                raise AttributeError('setting absolute {0} is not supported'.format(key))
            elif key !='tzinfo':
                raise AttributeError('unknown attribute: "{0}"'.format(key))

        # core datetime does not support quarters, translate to months.
        relative_kwargs.setdefault('months', 0)
        relative_kwargs['months'] += relative_kwargs.pop('quarters', 0) * self._MONTHS_PER_QUARTER

        current = self.datetime.replace(**absolute_kwargs)
        current += relativedelta(**relative_kwargs) # TODO: DEPRECATED

        tzinfo = kwargs.get('tzinfo')

        if tzinfo is not None:
            tzinfo = self._get_tzinfo(tzinfo)
            current = current.replace(tzinfo=tzinfo)

        return self.fromdatetime(current)

    def shift(self, **kwargs):
        ''' Returns a new :class:`Arrow <arrow.arrow.Arrow>` object with attributes updated
        according to inputs.

        Use pluralized property names to shift their current value relatively:

        >>> import arrow
        >>> arw = arrow.utcnow()
        >>> arw
        <Arrow [2013-05-11T22:27:34.787885+00:00]>
        >>> arw.shift(years=1, months=-1)
        <Arrow [2014-04-11T22:27:34.787885+00:00]>

        Day-of-the-week relative shifting can use either Python's weekday numbers
        (Monday = 0, Tuesday = 1 .. Sunday = 6) or using dateutil.relativedelta's
        day instances (MO, TU .. SU).  When using weekday numbers, the returned
        date will always be greater than or equal to the starting date.

        Using the above code (which is a Saturday) and asking it to shift to Saturday:

        >>> arw.shift(weekday=5)
        <Arrow [2013-05-11T22:27:34.787885+00:00]>

        While asking for a Monday:

        >>> arw.shift(weekday=0)
        <Arrow [2013-05-13T22:27:34.787885+00:00]>

        '''

        relative_kwargs = {}

        for key, value in kwargs.items():

            if key in self._ATTRS_PLURAL or key in ['weeks', 'quarters', 'weekday']:
                relative_kwargs[key] = value
            else:
                raise AttributeError()

        # core datetime does not support quarters, translate to months.
        relative_kwargs.setdefault('months', 0)
        relative_kwargs['months'] += relative_kwargs.pop('quarters', 0) * self._MONTHS_PER_QUARTER

        current = self.datetime + relativedelta(**relative_kwargs)

        return self.fromdatetime(current)

    def to(self, tz):
        ''' Returns a new :class:`Arrow <arrow.arrow.Arrow>` object, converted
        to the target timezone.

        :param tz: A :ref:`timezone expression <tz-expr>`.

        Usage::

            >>> utc = arrow.utcnow()
            >>> utc
            <Arrow [2013-05-09T03:49:12.311072+00:00]>

            >>> utc.to('US/Pacific')
            <Arrow [2013-05-08T20:49:12.311072-07:00]>

            >>> utc.to(tz.tzlocal())
            <Arrow [2013-05-08T20:49:12.311072-07:00]>

            >>> utc.to('-07:00')
            <Arrow [2013-05-08T20:49:12.311072-07:00]>

            >>> utc.to('local')
            <Arrow [2013-05-08T20:49:12.311072-07:00]>

            >>> utc.to('local').to('utc')
            <Arrow [2013-05-09T03:49:12.311072+00:00]>

        '''

        if not isinstance(tz, tzinfo):
            tz = parser.TzinfoParser.parse(tz)

        dt = self.datetime.astimezone(tz)

        return self.__class__(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second,
            dt.microsecond, dt.tzinfo)

    # @profile
    def span(self, frame, count=1, magnitude = 1):
        ''' Returns two new :class:`Arrow <arrow.arrow.Arrow>` objects, representing the timespan
        of the :class:`Arrow <arrow.arrow.Arrow>` object in a given timeframe.

        :param frame: the timeframe.  Can be any ``datetime`` property (day, hour, minute...).
        :param count: (optional) the number of frames to span.

        Supported frame values: year, quarter, month, week, day, hour, minute, second.

        Usage::

            >>> arrow.utcnow()
            <Arrow [2013-05-09T03:32:36.186203+00:00]>

            >>> arrow.utcnow().span('hour')
            (<Arrow [2013-05-09T03:00:00+00:00]>, <Arrow [2013-05-09T03:59:59.999999+00:00]>)

            >>> arrow.utcnow().span('day')
            (<Arrow [2013-05-09T00:00:00+00:00]>, <Arrow [2013-05-09T23:59:59.999999+00:00]>)

            >>> arrow.utcnow().span('day', count=2)
            (<Arrow [2013-05-09T00:00:00+00:00]>, <Arrow [2013-05-10T23:59:59.999999+00:00]>)

        '''
        frame_absolute, frame_relative, relative_steps, _ = self._get_frames(frame)
        print("-------", frame_absolute, frame_relative, relative_steps)
        if frame_absolute == 'week':
            attr = 'day'
        elif frame_absolute == 'quarter':
            attr = 'month'
        elif frame_absolute == 'year':
            attr = 'year'
        else:
            attr = frame_relative

        index = self._ATTRS.index(attr)
        frames = self._ATTRS[:index + 1]

        values = [getattr(self, f) for f in frames]
        for i in range(3 - len(values)):
            values.append(1)

        if frame_relative == 'second' and frame_relative != frame_absolute:
            x = ureg.parse_expression(frame_absolute).to_base_units()
            decimals = int(abs(log10(x.magnitude)))
            # print("decimals", decimals)
            subseconds = Decimal(str(self.subseconds)[:decimals + 2])
            # print("subseconds A", subseconds)
            b = (ureg.parse_expression(frame_absolute) * magnitude).to_base_units()
            subseconds = int(subseconds / Decimal(str(b.magnitude))) * Decimal(b.magnitude)
        elif frame_relative == 'year':
            values[0] = (values[0] // magnitude) * magnitude
            subseconds = Decimal("0.0") #self.subseconds
        elif len(values) > 3:
            print("d2")
            values[-1] = int(values[-1] / magnitude) * magnitude
            subseconds = Decimal("0.0") #self.subseconds
        else:
            print("UNKNOOOOWNNN")
            subseconds = Decimal("0.0")  # self.subseconds


        floor = self.__class__.fromDatetime(datetime(*values, tzinfo=self.tzinfo), subseconds, precision=self.precision)

        # if frame_absolute == 'week':
        #     floor = floor + relativedelta(days=-(self.isoweekday() - 1))
        # elif frame_absolute == 'quarter':
        #     floor = floor + relativedelta(months=-((self.month - 1) % 3))

        if frame_relative == "second" and relative_steps < 1:
            x = (ureg.parse_expression(frame_relative) * relative_steps).to("second")
            d = Decimal(str(x.magnitude))
            ceil = (floor
                    + Decimal(str(x.magnitude))
                    + Decimal("-1E-{}".format(self.precision)))
        else:
            # print(".....", floor, type(floor))
            # print("A ", relativedelta(**{frame_relative + "s": int(count * relative_steps)}))
            # print("B ", floor + relativedelta(**{frame_relative + "s": int(count * relative_steps)}))
            # print("C ", floor + relativedelta(**{frame_relative + "s": int(count * relative_steps)}))
            # print("", Decimal("-1E-{}".format(self.precision)))
            ceil = (floor
                    + relativedelta(**{frame_relative + "s": int(count * relative_steps)})
                    + Decimal("-1E-{}".format(self.precision)))

        return floor, ceil

    def floor(self, frame, magnitude = 1):
        ''' Returns a new :class:`Arrow <arrow.arrow.Arrow>` object, representing the "floor"
        of the timespan of the :class:`Arrow <arrow.arrow.Arrow>` object in a given timeframe.
        Equivalent to the first element in the 2-tuple returned by
        :func:`span <arrow.arrow.Arrow.span>`.

        :param frame: the timeframe.  Can be any ``datetime`` property (day, hour, minute...).

        Usage::

            >>> arrow.utcnow().floor('hour')
            <Arrow [2013-05-09T03:00:00+00:00]>
        '''

        return self.span(frame, magnitude=magnitude)[0]

    def ceil(self, frame, magnitude = 1):
        ''' Returns a new :class:`Arrow <arrow.arrow.Arrow>` object, representing the "ceiling"
        of the timespan of the :class:`Arrow <arrow.arrow.Arrow>` object in a given timeframe.
        Equivalent to the second element in the 2-tuple returned by
        :func:`span <arrow.arrow.Arrow.span>`.

        :param frame: the timeframe.  Can be any ``datetime`` property (day, hour, minute...).

        Usage::

            >>> arrow.utcnow().ceil('hour')
            <Arrow [2013-05-09T03:59:59.999999+00:00]>
        '''

        return self.span(frame, magnitude=magnitude)[1]

    def isBeginOfFrame(self, frame):
        return self.floor(frame) == self

    def isBeginOfDay(self):
        return self.floor("day") == self

    def beginOfDay(self):
        return self.floor("day")

    def beginOfMonth(self):
        return self.floor("month")

    def format(self, fmt='YYYY-MM-DD HH:mm:ssZZ', locale='en_us'):
        ''' Returns a string representation of the :class:`Arrow <arrow.arrow.Arrow>` object,
        formatted according to a format string.

        :param fmt: the format string.

        Usage::

            >>> arrow.utcnow().format('YYYY-MM-DD HH:mm:ss ZZ')
            '2013-05-09 03:56:47 -00:00'

            >>> arrow.utcnow().format('X')
            '1368071882'

            >>> arrow.utcnow().format('MMMM DD, YYYY')
            'May 09, 2013'

            >>> arrow.utcnow().format()
            '2013-05-09 03:56:47 -00:00'

        '''

        return formatter.DateTimeFormatter(locale).format(self.datetime, fmt)


    def humanize(self, other=None, locale='en_us', only_distance=False, granularity='auto'):
        ''' Returns a localized, humanized representation of a relative difference in time.

        :param other: (optional) an :class:`Arrow <arrow.arrow.Arrow>` or ``datetime`` object.
            Defaults to now in the current :class:`Arrow <arrow.arrow.Arrow>` object's timezone.
        :param locale: (optional) a ``str`` specifying a locale.  Defaults to 'en_us'.
        :param only_distance: (optional) returns only time difference eg: "11 seconds" without "in" or "ago" part.
        :param granularity: (optional) defines the precision of the output. Set it to strings 'second', 'minute', 'hour', 'day', 'month' or 'year'.
        Usage::

            >>> earlier = arrow.utcnow().shift(hours=-2)
            >>> earlier.humanize()
            '2 hours ago'

            >>> later = later = earlier.shift(hours=4)
            >>> later.humanize(earlier)
            'in 4 hours'

        '''

        locale = locales.get_locale(locale)

        if other is None:
            utc = datetime.utcnow().replace(tzinfo=dateutil_tz.tzutc())
            dt = utc.astimezone(self.datetime.tzinfo)

        elif isinstance(other, Arrow):
            dt = other.datetime

        elif isinstance(other, datetime):
            if other.tzinfo is None:
                dt = other.replace(tzinfo=self.datetime.tzinfo)
            else:
                dt = other.astimezone(self.datetime.tzinfo)

        else:
            raise TypeError()

        delta = int(util.total_seconds(self.datetime - dt))
        sign = -1 if delta < 0 else 1
        diff = abs(delta)
        delta = diff

        if granularity=='auto':
            if diff < 10:
                return locale.describe('now', only_distance=only_distance)

            if diff < 45:
                seconds = sign * delta
                return locale.describe('seconds', seconds, only_distance=only_distance)

            elif diff < 90:
                return locale.describe('minute', sign, only_distance=only_distance)
            elif diff < 2700:
                minutes = sign * int(max(delta / 60, 2))
                return locale.describe('minutes', minutes, only_distance=only_distance)

            elif diff < 5400:
                return locale.describe('hour', sign, only_distance=only_distance)
            elif diff < 79200:
                hours = sign * int(max(delta / 3600, 2))
                return locale.describe('hours', hours, only_distance=only_distance)

            elif diff < 129600:
                return locale.describe('day', sign, only_distance=only_distance)
            elif diff < 2160000:
                days = sign * int(max(delta / 86400, 2))
                return locale.describe('days', days, only_distance=only_distance)

            elif diff < 3888000:
                return locale.describe('month', sign, only_distance=only_distance)
            elif diff < 29808000:
                self_months = self.datetime.year * 12 + self.datetime.month
                other_months = dt.year * 12 + dt.month

                months = sign * int(max(abs(other_months - self_months), 2))

                return locale.describe('months', months, only_distance=only_distance)

            elif diff < 47260800:
                return locale.describe('year', sign, only_distance=only_distance)
            else:
                years = sign * int(max(delta / 31536000, 2))
                return locale.describe('years', years, only_distance=only_distance)

        else:
            if granularity == 'second':
                delta = sign * delta
                if(abs(delta) < 2):
                    return locale.describe('now', only_distance=only_distance)
            elif granularity == 'minute':
                delta = sign * delta / float(60)
            elif granularity == 'hour':
                delta = sign * delta / float(60*60)
            elif granularity == 'day':
                delta = sign * delta / float(60*60*24)
            elif granularity == 'month':
                delta = sign * delta / float(60*60*24*30.5)
            elif granularity == 'year':
                delta = sign * delta / float(60*60*24*365.25)
            else:
                raise AttributeError('Error. Could not understand your level of granularity. Please select between \
                "second", "minute", "hour", "day", "week", "month" or "year"')

            if(trunc(abs(delta)) != 1):
                granularity += 's'
            return locale.describe(granularity, delta, only_distance=only_distance)
    # math

    def __add__(self, other):
        if isinstance(other, timedelta):
            return self.__class__(value=self.value + Decimal(other.total_seconds()), precision=self.precision)
        elif isinstance(other, relativedelta):
            # print("relativedelta __add__")
            # print("datetime", self.datetime)
            # print("other", other)
            dt = self.datetime + other
            # print("dt", dt)

            return self.__class__.fromDatetime(dt, precision=self.precision)

        elif isinstance(other, Decimal):
            return self.__class__(value=self.value + other, precision=self.precision)

        return NotImplemented

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):

        if isinstance(other, (timedelta, relativedelta)):
            return self.fromdatetime(self.datetime - other, self.datetime.tzinfo)
        elif isinstance(other, Decimal):
            return self.__class__(value=self.value - other, precision=self.precision)
        elif isinstance(other, datetime):
            return self.datetime - other

        elif isinstance(other, Arrow):
            return self.datetime - other.datetime

        return NotImplemented

    def __rsub__(self, other):

        if isinstance(other, datetime):
            return other - self.datetime

        return NotImplemented


    # comparisons

    def __eq__(self, other):

        if not isinstance(other, (Arrow, datetime)):
            return False

        return self.value == other.value

    def __ne__(self, other):

        if not isinstance(other, (Arrow, datetime)):
            return True

        return not self.__eq__(other)

    def __gt__(self, other):

        if not isinstance(other, (Arrow, datetime)):
            return NotImplemented

        return self.value > other.value #self.datetime > self._get_datetime(other)

    def __ge__(self, other):

        if not isinstance(other, (Arrow, datetime)):
            return NotImplemented

        return self.value >= other.value #self.datetime >= self._get_datetime(other)

    def __lt__(self, other):

        if not isinstance(other, (Arrow, datetime)):
            return NotImplemented

        return self.value < other.value #self.datetime < self._get_datetime(other)

    def __le__(self, other):
        if not isinstance(other, (Arrow, datetime)):
            return NotImplemented

        return self.value <= other.value #self.datetime <= self._get_datetime(other)

    def __cmp__(self, other):
        if sys.version_info[0] < 3: # pragma: no cover
            if not isinstance(other, (Arrow, datetime)):
                raise TypeError('can\'t compare \'{0}\' to \'{1}\''.format(
                    type(self), type(other)))



    # datetime methods

    def date(self):
        ''' Returns a ``date`` object with the same year, month and day. '''

        return self.datetime.date()

    def time(self):
        ''' Returns a ``time`` object with the same hour, minute, second, microsecond. '''

        return self.datetime.time()

    def timetz(self):
        ''' Returns a ``time`` object with the same hour, minute, second, microsecond and
        tzinfo. '''

        return self.datetime.timetz()

    def astimezone(self, tz):
        ''' Returns a ``datetime`` object, converted to the specified timezone.

        :param tz: a ``tzinfo`` object.

        '''

        return self.datetime.astimezone(tz)

    def utcoffset(self):
        ''' Returns a ``timedelta`` object representing the whole number of minutes difference from
        UTC time. '''

        return self.datetime.utcoffset()

    def dst(self):
        ''' Returns the daylight savings time adjustment. '''

        return self.datetime.dst()

    def timetuple(self):
        ''' Returns a ``time.struct_time``, in the current timezone. '''

        return self.datetime.timetuple()

    def utctimetuple(self):
        ''' Returns a ``time.struct_time``, in UTC time. '''

        return self.datetime.utctimetuple()

    def toordinal(self):
        ''' Returns the proleptic Gregorian ordinal of the date. '''

        return self.datetime.toordinal()

    def weekday(self):
        ''' Returns the day of the week as an integer (0-6). '''

        return self.datetime.weekday()

    def isoweekday(self):
        ''' Returns the ISO day of the week as an integer (1-7). '''

        return self.datetime.isoweekday()

    def isocalendar(self):
        ''' Returns a 3-tuple, (ISO year, ISO week number, ISO weekday). '''

        return self.datetime.isocalendar()

    def isoformat(self, sep='T'):
        '''Returns an ISO 8601 formatted representation of the date and time. '''

        return self.datetime.isoformat(sep)

    def ctime(self):
        ''' Returns a ctime formatted representation of the date and time. '''

        return self.datetime.ctime()

    def strftime(self, format):
        ''' Formats in the style of ``datetime.strptime``.

        :param format: the format string.

        '''

        return self.datetime.strftime(format)

    def for_json(self):
        '''Serializes for the ``for_json`` protocol of simplejson.'''

        return self.isoformat()

    # internal tools.

    @staticmethod
    def _get_tzinfo(tz_expr):

        if tz_expr is None:
            return dateutil_tz.tzutc()
        if isinstance(tz_expr, tzinfo):
            return tz_expr
        else:
            try:
                return parser.TzinfoParser.parse(tz_expr)
            except parser.ParserError:
                raise ValueError('\'{0}\' not recognized as a timezone'.format(
                    tz_expr))

    @classmethod
    def _get_datetime(cls, expr):

        if isinstance(expr, Arrow):
            return expr.datetime

        if isinstance(expr, datetime):
            return expr

        try:
            expr = float(expr)
            return cls.utcfromtimestamp(expr).datetime
        except:
            raise ValueError(
                '\'{0}\' not recognized as a timestamp or datetime'.format(expr))

    # @classmethod
    # def _get_base_frames(cls):
    #     return {
    #             'year' : ('year', 'years', 1),
    #             'quarter' : ('quarter', 'months', 3),
    #             'month' : ('month', 'months', 1),
    #             'week' : ('week', 'weeks', 1),
    #             'day' : ('day', 'days', 1),
    #             'hour' : ('hour', 'hours', 1),
    #             'minute' : ('minute', 'minutes', 1),
    #             'second' : ('second', 'seconds', 1),
    #             }

    @classmethod
    def _get_frames(cls, frame):
        unit = frame
        base_unit = frame

        magnitude = 1
        subs = [1]
        if frame.endswith("second"):
            x = ureg.parse_expression(frame).to(ureg.second)
            base_unit = str(x.units)
            magnitude = x.magnitude
            if base_unit == unit:
                subs = [1, 5, 15]
            else:
                subs = [1, 5, 10, 50, 100, 500]
        elif frame.endswith("minute"):
            subs = [1, 5, 15]
        elif frame.endswith("hour"):
            subs = [1, 12]
        elif frame.endswith("year") or frame in ["decade", "century", "millenium", "millenia", "milenia", "milenium"]:
            x = ureg.parse_expression(frame).to(ureg.year)
            base_unit = str(x.units)
            magnitude = x.magnitude
            subs = [1, 5, 10, 50, 100, 500]

        return unit, base_unit, magnitude, subs
        # if base_unit in [*cls._get_base_frames()]:
        #     return (*cls._get_base_frames()[base_unit], magnitude)
        #
        # supported = ', '.join(cls._ATTRS + ['week', 'weeks'] + ['quarter', 'quarters'])
        # raise AttributeError('range/span over frame {0} not supported. Supported frames: {1}'.format(base_unit, supported))

    @classmethod
    def _get_suffix(cls, frame):
        if frame == "year":
            return "y"
        elif frame == "months":
            return "m"
        elif frame == "day":
            return "d"
        elif frame == "hour":
            return "h"
        elif frame == "minute":
            return "'"
        elif frame.endswith("second"):
            if len(frame) > len("second"):
                return frame[0]
            return "\""

        return ""

    @classmethod
    def _get_iteration_params(cls, end, limit):

        if end is None:

            if limit is None:
                raise Exception('one of \'end\' or \'limit\' is required')

            return cls.max, limit

        else:
            if limit is None:
                return end, sys.maxsize
            return end, limit

    @staticmethod
    def _get_timestamp_from_input(timestamp):

        try:
            return float(timestamp)
        except:
            raise ValueError('cannot parse \'{0}\' as a timestamp'.format(timestamp))

Arrow.min = Arrow.fromISO("0001-01-01 00:00:00Z")
Arrow.max = Arrow.fromISO("9999-12-31 23:59:59Z.999999")